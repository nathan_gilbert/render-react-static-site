#FROM kong:1.2rc1-centos
FROM kong:2.7.1-alpine
COPY kong.yml /
RUN cp /etc/kong/kong.conf.default /etc/kong/kong.conf
